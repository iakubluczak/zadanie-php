<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Support\Facades\DB;

class ProductRepository implements ProductRepositoryInterface
{
    public function all()
    {
        return Product::all();
    }

    public function addedToShoppingCart($shoppingCart)
    {
        if (is_null($shoppingCart)) {
            return [];
        }
        $products = $shoppingCart->products;
        $productsQuantity = [];
        foreach ($products as $product) {
            $productId = strval($product->id);
            if (isset($productsQuantity[$productId])) {
                $productsQuantity[$productId]['quantity']++;
            } else {
                $productsQuantity[$productId]['product'] = $product;
                $productsQuantity[$productId]['quantity'] = 1;
            }
        }
        return $productsQuantity;
    }

    public function getProductIdToDetachInShoppingCart($productId, $shoppingCartId)
    {
        return DB::table('product_shopping_cart')
            ->orderBy('created_at', 'desc')
            ->where('product_id', $productId)
            ->where('shopping_cart_id', $shoppingCartId)
            ->pluck('id')
            ->first();
    }
}
