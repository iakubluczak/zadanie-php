<?php

namespace App\Repositories;

use App\Models\ShoppingCart;
use App\Repositories\Interfaces\ShoppingCartRepositoryInterface;

class ShoppingCartRepository implements ShoppingCartRepositoryInterface
{
    public function getCurrent($request)
    {
        return $request->session()->has('shoppingCartId') ?
            ShoppingCart::find($request->session()->get('shoppingCartId')) :
            null;
    }

    public function getCurrentOrCreateNew($request)
    {
        $shoppingCart = $this->getCurrent($request);
        if (is_null($shoppingCart)) {
            $shoppingCart = new ShoppingCart();
            $shoppingCart->user_id = $request->user()->id;
            $shoppingCart->save();
            $request->session()->put('shoppingCartId', $shoppingCart->id);
        }
        return $shoppingCart;
    }
}
