<?php

namespace App\Repositories\Interfaces;

interface ShoppingCartRepositoryInterface
{
    public function getCurrent($request);

    public function getCurrentOrCreateNew($request);
}
