<?php

namespace App\Repositories\Interfaces;

interface ProductRepositoryInterface
{
    public function all();

    public function addedToShoppingCart($shoppingCart);

    public function getProductIdToDetachInShoppingCart($productId, $shoppingCartId);
}
