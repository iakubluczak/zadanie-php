<?php

namespace App\Console\Commands;

use App\Models\ShoppingCart;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeleteOldShoppingCarts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shoppingcarts:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes dated shopping carts. It\'s not necessary - containing old customer\'s
    shopping carts and analyze them is also worthwhile. The goal of removal them is to avoid database overloading.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ShoppingCart::where('created_at', '<', Carbon::now()->subDay())->delete();
    }
}
