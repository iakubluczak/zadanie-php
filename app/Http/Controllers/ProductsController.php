<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Repositories\Interfaces\ShoppingCartRepositoryInterface;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    private $productRepository;
    private $shoppingCartRepository;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        ShoppingCartRepositoryInterface $shoppingCartRepository
    ) {
        $this->productRepository = $productRepository;
        $this->shoppingCartRepository = $shoppingCartRepository;
    }

    public function index(Request $request)
    {
        $products = $this->productRepository->all();
        $shoppingCart = $this->shoppingCartRepository->getCurrent($request);
        $shoppingCartProducts = $this->productRepository->addedToShoppingCart($shoppingCart);
        return view(
            'products',
            [
                'products' => $products,
                'shoppingCartProducts' => $shoppingCartProducts,
            ]
        );
    }

    public function addProductToCart(Request $request, $id)
    {
        $shoppingCart = $this->shoppingCartRepository->getCurrentOrCreateNew($request);
        $shoppingCart->products()->attach($id);
        return redirect()->route('dashboard');
    }

    public function removeProductFromCart(Request $request, $id)
    {
        $shoppingCart = $this->shoppingCartRepository->getCurrent($request);
        $productIdInPivot = $this->productRepository->getProductIdToDetachInShoppingCart($id, $shoppingCart->id);
        $shoppingCart->products()->wherePivot('id', '=', $productIdInPivot)->detach();
        return redirect()->route('dashboard');
    }

    public function removeAllSpecificProductsFromCart(Request $request, $id)
    {
        $shoppingCart = $this->shoppingCartRepository->getCurrent($request);
        $shoppingCart->products()->detach($id);
        return redirect()->route('dashboard');
    }
}
