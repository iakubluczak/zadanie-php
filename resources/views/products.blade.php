<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Product list') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-2xl mx-auto sm:px-2 lg:px-2">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h2>Product list:</h2>
                    <table class="table-auto">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                                <tr>
                                    <td class="pr-4">{{ $product->name }}</td>
                                    <td>
                                        <form action="{{ route('add-to-cart', ['id' => $product->id]) }}" method="post">
                                            @csrf
                                            <x-button>
                                                To cart
                                            </x-button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
                @if(!empty($shoppingCartProducts))
                <div class="p-6 bg-white border-b border-gray-200">
                    <h2>Shopping cart:</h2>
                    <table class="table-fixed">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th class="pr-10"></th>
                            <th>Quantity</th>
                            <th class="pr-10"></th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($shoppingCartProducts as $id => $productsQuantity)
                            <tr>
                                <td>{{ $productsQuantity['product']->name}}</td>
                                <td>
                                    <form action="{{ route('remove-from-cart', ['id' => $id]) }}" method="post">
                                        @csrf
                                        <x-button> - </x-button>
                                    </form>
                                </td>
                                <td class="flex justify-center">{{ $productsQuantity['quantity'] }}</td>
                                <td>
                                    <form action="{{ route('add-to-cart', ['id' => $id]) }}" method="post">
                                        @csrf
                                        <x-button> + </x-button>
                                    </form>
                                </td>
                                <td class="flex justify-center">
                                    <form action="{{ route('remove-all-from-cart', ['id' => $id]) }}" method="post">
                                        @csrf
                                        <button type="submit"> × </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>
</x-app-layout>

c
