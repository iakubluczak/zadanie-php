<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', "App\Http\Controllers\ProductsController@index")
        ->name('dashboard');

    Route::post('/shopping-cart/{id}', "App\Http\Controllers\ProductsController@addProductToCart")
        ->name('add-to-cart');

    Route::post('/shopping-cart/remove/{id}', "App\Http\Controllers\ProductsController@removeProductFromCart")
        ->name('remove-from-cart');

    Route::post(
        '/shopping-cart/remove-all/{id}',
        "App\Http\Controllers\ProductsController@removeAllSpecificProductsFromCart"
    )
        ->name('remove-all-from-cart');
});

require __DIR__.'/auth.php';
